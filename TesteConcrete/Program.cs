﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace TesteConcrete
{
    class Program
    {
        static ArrayList names = new ArrayList();        
        
        static bool loadArray(String fileIn) /// This function  verifies if the file exists, and then load all the content of the file into the array.
        {
            if (!File.Exists(fileIn)) return false;

            StreamReader readfile = new StreamReader(fileIn, Encoding.Default);
            String line;
            String[] data;

            line = readfile.ReadLine();

            while (line != null)
            {
                data = line.Split(';');
                names.Add(data[0]);
                line = readfile.ReadLine();
            }

            readfile.Close();

            return true;
        }

        static string biggerName() /// This function return the bigger name and how much characters are in the name.
        {
            string bigger = "";
            int sizename = 0;
            int prev_sizename = 0;

            foreach (string s in names)
            {
                sizename = nameLength(s);

                if (sizename >= prev_sizename)
                {
                    bigger = s + ", " + sizename;
                    prev_sizename = sizename;
                }
            }

            return bigger;
        }

        static int nameLength(string s) /// This function receive a string, remove all spaces of it and return the size of the string.
        {
            string removedspace;
            removedspace = s.Replace(" ", "");
            return removedspace.Length;
        }


        static void Main(string[] args)
        {
            string fileNames = @"names.txt";

            if (loadArray(fileNames))
            {
                Console.WriteLine("Arquivo encontrado!");
                Console.WriteLine(biggerName());
                Console.ReadKey();
            }

            else
            {
                Console.WriteLine("Arquivo " + names + " não foi encontrado!");
            }

        }
    }
}
